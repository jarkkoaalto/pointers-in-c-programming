/*
 ============================================================================
 Name        : Pointers-in-c-programming.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : intorduction of Pointers
 ============================================================================
*/

#include <stdio.h>
#include <string.h>

int main() {
	int a [5] = {1,2,3,4,5};
	int *p;
	p = &a[0];
	printf("%d\n",p); // address of a[0]
	p = &a[1];
	printf("%d\n",p); // address of a[1]
	p = &a[2];
	printf("%d\n",p); // address of a[2]
	p = &a[3];
	printf("%d\n",p); // address of a[3]
	p = &a[4];
	printf("%d\n",p); // address of a[4]

	int i;
	int b[5] = {1,2,3,4,5};
	int *u;
	u = b;

	for(i=0;i<5;i++){
		printf("%d\n", *u); // 1,2,3,4,5
		u++; // if commented print 1,1,1,1,1
	}
	return 0;
}

