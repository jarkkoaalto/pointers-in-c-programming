/*
 ============================================================================
 Name        : Pointers-in-c-programming.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Dynamic Memory Allocation
 ============================================================================
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
	char *a;

	a = malloc(20 * sizeof(char));
		if(a == NULL)
		{
			printf("Couldn't able to allocate requested memory\n");
		}
		else
		{
			strcpy(a, "Dynamic memory allocation");
		}
		printf("Dynamically allocated memory content : %s\n", a);
	a = realloc(a,100 * sizeof(char));
		if(a == NULL)
		{
			printf("Couldn't able to allocate requested memory\n");
		}
		else
		{
			strcpy(a, "Dynamic memory reallocation");
		}
		printf("Dynamically reallocated memory content : %s\n", a);
	free(a);

	return 0;
}




