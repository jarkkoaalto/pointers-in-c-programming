/*
 ============================================================================
 Name        : Pointers-in-c-programming.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Pointer and Functions
 ============================================================================
*/

#include <stdio.h>
#include <string.h>

void swap(int *a, int *b);

int main() {
	int num1=4, num2=10;
	swap(&num1, &num2);
	printf("Number1 = %d\n", num1);
	printf("Number2 = %d\n", num2);
	return 0;
}

void swap(int *a, int *b){
	int temp;
	temp =*a;
	*a=*b;
	*b=temp;
}


