/*
 ============================================================================
 Name        : Pointers-in-c-programming.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : intorduction of Pointers
 ============================================================================
 */

#include <stdio.h>
#include <string.h>

int main() {
	int a = 55;
	int *b; // pointer
	b = &a; // memory address

	printf("value of a = %d\n",a);
	printf("address of a = %d", &a);
	return 0;
}
