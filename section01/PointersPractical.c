/*
 ============================================================================
 Name        : Pointers-in-c-programming.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : intorduction of Pointers
 ============================================================================
*/

#include <stdio.h>
#include <string.h>

int main() {
	int *pc,c;
	c = 22;

	printf("Address of c:%d\n", &c);
	printf("Value of c:%d\n\n", c);
	pc=&c;
	printf("Address of pointer pc:%d\n", pc);
	printf("Content of pointer pc:%d\n\n",*pc);
	c =11;
	printf("Address of pointer pc:%d\n",pc);
	printf("Content of pointer pc:%d\n\n",*pc);
	*pc=2;
	printf("Address of c:%d\n", &c);
	printf("Value of c:%d\n\n",c);
	return 0;
}

